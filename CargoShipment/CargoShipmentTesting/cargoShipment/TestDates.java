package cargoShipment;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestDates {

	@Test
	public void testWorkingDays() {
		ShipmentDate sd = new ShipmentDate("22-09-2020 10:00:00", 30, "+05:30");
		assertEquals("tuesday 00:00", sd.getDate());			
	}
	
	@Test
	public void testHolidays() {
		ShipmentDate sd = new ShipmentDate("26-09-2020 07:05:40", 27 , "+05:30");
		assertEquals("monday 12:00", sd.getDate());			
	}

	@Test
	public void testPublicHolidays() {
		// january 1st
		ShipmentDate sd = new ShipmentDate("01-01-2020 13:09:10", 45, "+05:30");
		assertEquals("sunday 12:00", sd.getDate());	
		
		//august 15th
		sd = new ShipmentDate("15-08-2020 19:15:00", 14, "+05:30");
		assertEquals("tuesday 00:00", sd.getDate());
		
		//january 26th
		sd = new ShipmentDate("26-01-2020 23:11:00", 39, "+05:30");
		assertEquals("thursday 12:00", sd.getDate());	
	}
	
	@Test
	public void testLeapYear(){
		ShipmentDate sd = new ShipmentDate("28-02-2020 10:00:00", 40, "+05:30");
		assertEquals("thursday 00:00", sd.getDate());	
	}
	
	@Test
	public void testY2K(){
		ShipmentDate sd = new ShipmentDate("31-12-1999 15:00:00", 17, "+05:30");
		assertEquals("sunday 00:00", sd.getDate());	
	}
	
	@Test
	public void testTimeZones(){
		ShipmentDate sd = new ShipmentDate("31-12-1999 15:00:00", 17, "+08:00");
		assertEquals("sunday 02:30", sd.getDate());	
	}
	
	@Test
	public void testGreenichTime(){
		ShipmentDate sd = new ShipmentDate("31-12-1999 15:00:00", 17, "+00:00");
		assertEquals("saturday 18:30", sd.getDate());	
	}
	
}
