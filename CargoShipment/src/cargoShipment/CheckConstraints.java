package cargoShipment;

import java.time.LocalDateTime;
import java.util.List;

public class CheckConstraints{
	public boolean isHoliday(LocalDateTime dateTime){
		boolean weekDay = isWeekDay(dateTime);
		boolean publicHoliday = isPublicHoliday(dateTime);
		return ( weekDay || publicHoliday );

	}

	public boolean isWeekDay(LocalDateTime dateTime){
		String weekDay = dateTime.getDayOfWeek().name().toLowerCase();
		List<String> holidays = Holidays.HOLIDAYS.getValues();
		if(holidays.contains(weekDay)){
			return true;
		}
		return false;
	}

	public boolean isPublicHoliday(LocalDateTime dateTime){
		String monthDay = monthDayFormat(dateTime);

		List<String> publicHolidays = PublicHolidays.PUBLIC_HOLIDAYS.getValues();
		if(publicHolidays.contains(monthDay)){
			return true;
		}

		return false;
	}

	public String monthDayFormat(LocalDateTime dateTime){
		String month = dateTime.getMonth().name().toLowerCase();
		int day = dateTime.getDayOfMonth();
		return month + day;		
	}
}