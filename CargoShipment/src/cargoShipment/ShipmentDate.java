package cargoShipment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import cargoShipment.CheckConstraints;


public class ShipmentDate {
	int idealHrs;
	String startDate;
	String zone;

	public static void main(String[] args){
		ShipmentDate sd = new ShipmentDate("31-12-1999 15:00:00", 17, "+00:00");
        System.out.println("Shipment arrives on " + sd.getDate());
	}
	
	
	public String getDate(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(this.startDate, formatter);


        int idealMins = hrsToMinutes(this.idealHrs);
        int numberOfHrsPerDay = PerDayConstraints.HOURS.numberOfHrsPerDay;
        
        int noOfMins = hrsToMinutes(numberOfHrsPerDay);

        CheckConstraints check = new CheckConstraints();
        
        while( true ){
        	if(!check.isHoliday(dateTime)){
        		int remainingMins = hrsToMinutes(this.remainingHrsInTheDay(dateTime));
        		int minsToAdd = Math.min(remainingMins, noOfMins);
        		minsToAdd = Math.min(minsToAdd, idealMins);
        		dateTime = dateTime.plusHours(minsToAdd);
        		idealMins -= minsToAdd;
        	}
        	if(idealMins > 0)
        		dateTime = this.nextDayMidnight(dateTime);
        	else
        		break;
        }
        
        String correctZone = getZone(dateTime, this.zone);
        
        return correctZone;
	}

	
	
	public ShipmentDate(String startDate, int idealHrs, String zone){
		this.idealHrs = idealHrs;
		this.startDate = startDate;
		this.zone = zone;
	}
	
	
	public String getZone(LocalDateTime dateTime, String zone){
		ZonedDateTime zoneDateTime = dateTime.atZone(ZoneId.systemDefault());
        zoneDateTime = zoneDateTime.withZoneSameInstant(ZoneId.of(zone));
        
        String currTime = zoneDateTime.toLocalTime().toString();
        String day = zoneDateTime.getDayOfWeek().name().toLowerCase();
        return day + " " + currTime;
	}
	
	
	public int remainingHrsInTheDay(LocalDateTime dateTime){
		int hours = (int)dateTime.until( nextDayMidnight(dateTime), ChronoUnit.HOURS );
		return hours;
	}
	
	public int hrsToMinutes(int hrs){
		return hrs*60;
	}

	
	
	public LocalDateTime nextDayMidnight(LocalDateTime dateTime){
		LocalTime midnight = LocalTime.MIDNIGHT;
		LocalDate date = dateTime.toLocalDate().plusDays(1);
		return LocalDateTime.of(date, midnight);
	}
}